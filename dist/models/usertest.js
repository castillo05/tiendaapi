'use strict';

module.exports = (sequelize, DataTypes) => {
  const Usertest = sequelize.define('Usertest', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING
  }, {});

  Usertest.associate = function (models) {// associations can be defined here
  };

  return Usertest;
};