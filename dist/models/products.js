'use strict';

module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    code: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    product: DataTypes.STRING,
    category: DataTypes.INTEGER,
    description: DataTypes.INTEGER,
    price: DataTypes.FLOAT,
    image: DataTypes.STRING
  }, {});
  return Product;
};