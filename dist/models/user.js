'use strict';

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    mail: DataTypes.STRING,
    passwordHash: DataTypes.STRING,
    phoneNumber: DataTypes.INTEGER,
    lockoutEnabled: DataTypes.INTEGER,
    userName: DataTypes.STRING,
    name: DataTypes.STRING,
    lastName: DataTypes.STRING,
    RoleId: DataTypes.INTEGER
  }, {});

  User.associate = function (models) {// associations can be defined here
  };

  return User;
};