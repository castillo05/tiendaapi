'use strict';

var _models = _interopRequireDefault(require("./models"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = require('./app');

const port = process.env.PORT || 2019;

_models.default.sequelize.authenticate().then(() => {
  app.listen(port, function () {
    console.log('Conectado a MySql en el puerto ' + port);
  });
}).catch(err => {
  console.log('No se puede conectar a MSSQLS ', err);
});