'use strict';

const express = require('express');

const BodyParser = require('body-parser'); //Exportando las rutas


const userRoute = require('./routes/user.route');

const productRoute = require('./routes/product.route');

const app = express();
app.use(BodyParser.urlencoded({
  extended: false,
  limit: '50mb',
  parameterLimit: 50000
}));
app.use(BodyParser.json({
  limit: '50mb'
}));
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY,Origin,X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
}); //Rutas de Usuario

app.use('/api', userRoute); //Rutas de Productos

app.use('/api', productRoute);
app.get('/', (req, res) => {
  res.status(200).send('Api Tienda en linea');
});
module.exports = app;