'use strict';

const express = require('express');

const productController = require('../controllers/product.controller');

const auth = require('../middleware/authenticated');

const multipart = require('connect-multiparty');

const md_upload = multipart({
  uploadDir: './uploads'
});
const api = express.Router();
api.post('/product/productstore', productController.productStore);
api.post('/product/uploadimage/:codigo', [md_upload], productController.uploadImage);
api.get('/product/getproducts', productController.getProducts);
api.get('/product/getimage/:imageFile', productController.getImage);
module.exports = api;