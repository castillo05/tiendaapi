'use strict';

const express = require('express');

const userController = require('../controllers/user.controller');

const auth = require('../middleware/authenticated');

var api = express.Router();
api.post('/user/singup', userController.singUp);
api.post('/user/singin', userController.singIn);
module.exports = api;