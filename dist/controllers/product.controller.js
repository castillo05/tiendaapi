'use strict';

require("core-js/modules/es6.regexp.split");

var _models = _interopRequireDefault(require("../models"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const fs = require('fs');

const path = require('path'); // Metodo para agregar producto


function productStore(req, res) {
  const params = req.body;

  if (params.product == '' || params.category == '' || params.description == '' || params.price == '') {
    return res.status(200).send({
      message: 'Complete todos los campos...!'
    });
  }

  _models.default.Product.create({
    product: params.product,
    category: params.category,
    description: params.description,
    price: params.price
  }).then(product => {
    res.status(200).send({
      product: product
    });
  }).catch(error => {
    res.status(505).send({
      error: error
    });
  });
}

function uploadImage(req, res) {
  const productCode = req.params.codigo;
  var file_name = 'No Subida...';

  if (req.files) {
    var file_path = req.files.image.path;
    var file_split = file_path.split('\\');
    var file_name = file_split[1]; // var ext_split=file_name.split('\.');
    // var file_ext=ext_split[1];
    // if (file_ext=='png' || file_ext== 'jpg' || file_ext=='gif') {

    _models.default.Product.update({
      image: file_name
    }, {
      where: {
        code: productCode
      }
    }).then(productUpdate => {
      if (productUpdate) {
        return res.status(200).send({
          message: 'Imagen Cargada con Exito ' + file_name
        });
      }
    }).catch(error => {
      res.status(505).send({
        error: error
      });
    }); // } else {
    //     res.status(200).send({message:'Extension del archivo no valida'});
    // }


    console.log(file_path);
  } else {
    res.status(200).send({
      message: 'No has subido ninguna imagen'
    });
  }
} // Cargar Productos


function getProducts(req, res) {
  _models.default.Product.findAll({}).then(products => {
    res.status(200).send({
      products: products
    });
  }).catch(error => {
    res.status(500).send({
      error: error
    });
  });
} // Mostrar Imagenes del producto


function getImage(req, res) {
  var imageFile = req.params.imageFile;
  fs.exists("./uploads/" + imageFile, function (exists) {
    if (exists) {
      return res.sendFile(path.resolve('./uploads/' + imageFile));
    }

    res.status(200).send({
      message: 'No existe la imagen'
    });
  });
}

module.exports = {
  productStore,
  uploadImage,
  getProducts,
  getImage
};