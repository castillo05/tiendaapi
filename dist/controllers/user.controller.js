'use strict';

var _models = _interopRequireDefault(require("../models"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const passport = require('passport');

const localStrategy = require('passport-local');

const bcrypt = require('bcrypt');

const saltRounds = 10;

const env = require('../config');

const jwt = require('../services/jwt');

function singUp(req, res) {
  const params = req.body; //Validamos los campos para evitar errorres de entrada de datos

  if (params.mail == '' || params.passwordHash == '' || params.phoneNumber == '' || params.userName == '' || params.name == '' || params.lastName == '') {
    return res.status(200).send({
      message: 'Llenar todos los campos!'
    });
  } // Buscamos un usuario donde el email sea igual al introducido y si existe se lanza un sms.


  _models.default.User.findAll({
    where: {
      mail: params.mail
    }
  }).then(user => {
    if (user.length >= 1) {
      res.status(200).send({
        message: 'Lo sentimos este Email ya esta registrado'
      });
    } else if (!user) {} else {
      // Pero si no existe un email igual procedemos a crear el usuario
      if (params.passwordHash) {
        bcrypt.hash(params.passwordHash, saltRounds).then(hash => {
          _models.default.User.create({
            mail: params.mail,
            passwordHash: hash,
            phoneNumber: params.phoneNumber,
            lockoutEnabled: params.lockoutEnabled,
            userName: params.userName,
            name: params.name,
            lastName: params.lastName,
            roleId: params.roleId
          }).then(userStored => {
            res.status(200).send({
              userStored: userStored
            });
          }).catch(error => {
            res.status(500).send({
              error: error
            });
          });
        });
      } else {
        res.status(200).send({
          message: 'Introduzca la contraseña'
        });
      }
    }
  }).catch(error => {
    res.status(500).send({
      error: error
    });
  });
}

function singIn(req, res) {
  var mail = req.body.mail;
  var password = req.body.passwordHash;

  _models.default.User.findOne({
    where: {
      mail: mail
    },
    include: [{
      model: role,
      as: ''
    }]
  }).then(user => {
    if (user) {
      if (password != '') {
        bcrypt.compare(password, user.passwordHash).then(function (check) {
          if (check) {
            if (req.body.gethash) {
              return res.status(200).send({
                token: jwt.createToken(user)
              });
            } else {
              res.status(200).send({
                user: user
              });
            }
          } else {
            res.status(200).send({
              message: 'Contraseña Incorrecta'
            });
          }
        }).catch(error => {
          res.status(505).send({
            error: 'Error ' + error
          });
        });
      } else {
        res.status(200).send({
          message: 'Introduzca la contraseña'
        });
      }
    } else {
      res.status(200).send({
        message: 'Este email no esta registrado'
      });
    }
  }).catch(error => {
    res.status(505).send({
      error: 'Error ' + error
    });
  });
}

module.exports = {
  singUp,
  singIn
};