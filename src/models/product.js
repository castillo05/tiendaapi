'use strict';
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    id: {type:DataTypes.INTEGER,primaryKey:true,autoIncrement:true},
    code: DataTypes.INTEGER,
    product: DataTypes.STRING,
    category: DataTypes.INTEGER,
    description: DataTypes.STRING,
    price: DataTypes.FLOAT,
    image: DataTypes.STRING
  }, {});
  Product.associate = function(models) {
    // associations can be defined here
  };
  return Product;
};