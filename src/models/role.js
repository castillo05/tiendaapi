'use strict';
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    id: {type:DataTypes.INTEGER,primaryKey:true,autoIncrement:true},
    name: DataTypes.STRING,
    description: DataTypes.STRING
  }, {});
  Role.associate = function(models) {
    // associations can be defined here
  };
  return Role;
};