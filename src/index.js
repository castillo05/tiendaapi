'use strict'
const app=require('./app');
const port=process.env.PORT || 2019;
import db from './models';
 
 db.sequelize
     .authenticate()
     .then(()=>{
 
        app.listen(port,function () {
            console.log('Conectado a MySql en el puerto ' + port);
        });
     })
     .catch(err=>{
         console.log('No se puede conectar a MSSQLS ', err);
     });
 
 
    