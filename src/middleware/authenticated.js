'use strict'
const jwt = require('jwt-simple');
const moment = require('moment');
const secret = 'Tiendaenlinea2019';

exports.ensureAuth=function (req,res,next) {
    if (!req.headers.authorization) {
        return res.status(403).send({message:'Token no generado'});

    }

    var token=req.headers.authorization.replace(/['""']+/g,'');

    try {
        var payload = jwt.decode(token,secret);
        if (payload.exp<=moment().unix()) {
            return res.status(401).send({message:'El token ya expiro'});
        }
    } catch (ex) {
        console.log(ex);
        return res.status(404).send({message:'Token no valido'+ ex});
    }

    req.user=payload;
    next();
}