'use strict'
var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
var passport=require('passport');
var opts = {}

opts.jwtFromRequest=ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey='secret';
opts.issuer = 'accounts.examplesoft.com';
opts.audience = 'yoursite.net';

exports.module=JwtStrategy;