'use strict'

const jwt = require('jwt-simple');
const moment= require('moment');
const secret= 'Tiendaenlinea2019';

exports.createToken=function (user) {
    var payload={
        id:user.id,
        mail:user.mail,
        userName:user.userName,
        iat:moment().unix(),
        exp:moment().add(1,'days').unix
    }

    return jwt.encode(payload,secret);
}